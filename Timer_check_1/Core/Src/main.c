/*
 * main.c
 *
 *  Created on: 08 Jan 2021
 *      Author: Hemjal
 */


#include"string.h"
#include <ctype.h>
#include "main.h"
#include "stm32f4xx_hal.h"


void timer2_init(void);
void GPIO_Init(void);

TIM_HandleTypeDef htimer2;




void SystemClockConfig(void);
void Error_handler(void);



int main(void)
{
	HAL_Init();
	SystemClockConfig();
	GPIO_Init();
	timer2_init();

	HAL_TIM_Base_Start(&htimer2);

	while(1)
	{
		while( ! (TIM2->SR & TIM_SR_UIF) );
			TIM2->SR = 0;
			HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_3);  // Tested with logic analyzer and found ok
			//HAL_Delay(3000);
	}






	return 0;


}



void timer2_init(void)
{
	htimer2.Instance = TIM2;
	htimer2.Init.Prescaler = 24;
	htimer2.Init.Period = 6400-1;
	if(HAL_TIM_Base_Init(&htimer2)!=HAL_OK)
		Error_handler();

}


void SystemClockConfig(void)
{

}

void GPIO_Init(void)
{
    __HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitTypeDef ledgpio;
	ledgpio.Pin = GPIO_PIN_3;
	ledgpio.Mode = GPIO_MODE_OUTPUT_PP;
	ledgpio.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC,&ledgpio);
}


void Error_handler(void)
{


}
