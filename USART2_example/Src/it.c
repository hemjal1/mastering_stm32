/*
 * it.c
 *
 *  Created on: 28 Jul 2020
 *      Author: Hemjal
 */


void SysTick_Handler(void)
{
	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();

}

