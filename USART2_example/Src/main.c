/*
 * main.c
 *
 *  Created on: 28 Jul 2020
 *      Author: Hemjal
 */

#include "stm32f4xx_hal.h"
#include"string.h"
#include <ctype.h>
#include "main.h"

void UART2_Init(void);
void SystemClockConfig(void);
void Error_handler(void);


uint8_t to_capital(uint8_t ch);

UART_HandleTypeDef huart2;

char *my_data = "My name is khan\r\n";

int main(void)
{
	HAL_Init();
	SystemClockConfig();
	UART2_Init();

	uint16_t len = strlen(my_data);
	HAL_UART_Transmit(&huart2, (uint8_t *)my_data, len, HAL_MAX_DELAY);

	uint8_t r_dat, count=0;
	uint8_t buf[100];
	while(1)
	{
		HAL_UART_Receive(&huart2,&r_dat, 1, HAL_MAX_DELAY);
		if(r_dat == '\r')
			break;
		else
		{
			buf[count++]= to_capital(r_dat);
		}

	}

	buf[count++]= '\r';

	HAL_UART_Transmit(&huart2, (uint8_t *)buf, count, HAL_MAX_DELAY);

	while(1);



	return 0;


}


void UART2_Init(void)
{
	huart2.Instance = USART2;
	huart2.Init.BaudRate = 115200;
	huart2.Init.WordLength= UART_WORDLENGTH_8B;
	huart2.Init.StopBits= UART_STOPBITS_1;
	huart2.Init.Parity=UART_PARITY_NONE;
	huart2.Init.HwFlowCtl= UART_HWCONTROL_NONE;
	huart2.Init.Mode= UART_MODE_TX_RX;
	if(HAL_UART_Init(&huart2)!=HAL_OK)
	{

		//Do something
		Error_handler();
	}


}


uint8_t to_capital(uint8_t ch)
{
	if(ch>='a' && ch<='z')
		ch-=32;

	return ch;

}

void SystemClockConfig(void)
{

}


void Error_handler(void)
{


}
