/*
 * msp.c
 *
 *  Created on: 08 Jan 2021
 *      Author: Hemjal
 */


#include "main.h"

void HAL_MspInit(void)
{
	HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	SCB->SHCSR |= 0x7 <<16;
	HAL_NVIC_SetPriority(MemoryManagement_IRQn,0,0);
	HAL_NVIC_SetPriority(BusFault_IRQn,0,0);
	HAL_NVIC_SetPriority(UsageFault_IRQn,0,0);

}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htim)
{
	__HAL_RCC_TIM2_CLK_ENABLE();  // Enable timer clock
	HAL_NVIC_EnableIRQ( TIM2_IRQn ); // enable IRQ of tim2
	HAL_NVIC_SetPriority(TIM2_IRQn, 15, 0);

}

