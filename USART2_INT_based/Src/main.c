/*
 * main.c
 *
 *  Created on: 28 Jul 2020
 *      Author: Hemjal
 */

#include "stm32f4xx_hal.h"
#include"string.h"
#include <ctype.h>
#include "main.h"

#define TRUE 1
#define FALSE 0

void UART2_Init(void);
void SystemClockConfig(void);
void Error_handler(void);


uint8_t to_capital(uint8_t ch);

UART_HandleTypeDef huart2;

char *my_data = "My name is khan\r\n";

uint8_t r_dat, count=0, receive_cmplt = FALSE;
uint8_t buf[100];

int main(void)
{
	HAL_Init();
	SystemClockConfig();
	UART2_Init();

	uint16_t len = strlen(my_data);
	HAL_UART_Transmit(&huart2, (uint8_t *)my_data, len, HAL_MAX_DELAY);


	while(receive_cmplt!= TRUE)
		HAL_UART_Receive_IT(&huart2, &r_dat,1);

	while(1);



	return 0;


}


void UART2_Init(void)
{
	huart2.Instance = USART2;
	huart2.Init.BaudRate = 115200;
	huart2.Init.WordLength= UART_WORDLENGTH_8B;
	huart2.Init.StopBits= UART_STOPBITS_1;
	huart2.Init.Parity=UART_PARITY_NONE;
	huart2.Init.HwFlowCtl= UART_HWCONTROL_NONE;
	huart2.Init.Mode= UART_MODE_TX_RX;
	if(HAL_UART_Init(&huart2)!=HAL_OK)
	{

		//Do something
		Error_handler();
	}


}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)

{
	if(r_dat == '\r')
	{
		receive_cmplt= TRUE;
		buf[count++]= '\r';
		HAL_UART_Transmit(huart, buf, count, HAL_MAX_DELAY);
	}
	else
		buf[count++]= r_dat;



}

uint8_t to_capital(uint8_t ch)
{
	if(ch>='a' && ch<='z')
		ch-=32;

	return ch;

}

void SystemClockConfig(void)
{

}


void Error_handler(void)
{


}
